 package com.greatelerning.assignment.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
@Controller
public class ReadBookController {
	@PostMapping(value="read")
	public void userLogin(HttpServletRequest req, HttpSession hs, HttpServletResponse hp) throws IOException{
		String param=req.getParameter("rid");
		List li;
		if(hs.getAttribute("readTo")==null)
		{
			li=new ArrayList();
			li.add(param);
			hs.setAttribute("readTo", li);
		}else
		{
			li=(List) hs.getAttribute("readTo");
			if(!li.contains(param)){
			li.add(param);
		}
			hs.setAttribute("readTo", li);
			
		}

				hp.getWriter().print(li.size());
		
			}
}
