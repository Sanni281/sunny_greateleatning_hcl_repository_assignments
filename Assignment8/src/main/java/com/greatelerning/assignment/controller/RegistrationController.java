package com.greatelerning.assignment.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.greatelerning.assignment.bean.User;
import com.greatelerning.assignment.service.UserRegistrationService;

@Controller
public class RegistrationController {
@Autowired
UserRegistrationService userRegistrationService;
@PostMapping(value="registration")
 public ModelAndView userRegistration( HttpServletRequest req, HttpSession hs )
 {
	ModelAndView mav= new ModelAndView();
	String firstName = req.getParameter("firstName");// taking id text field value and convert into integer. 
	String lastName = req.getParameter("lastName");
	String email = req.getParameter("email");
	String password = req.getParameter("password");
	
	User user= new User();
	user.setFirstName(firstName);
	user.setLastName(lastName);
	user.setEmail(email);
	user.setPassword(password);
	userRegistrationService.storeUser(user);
	 mav.setViewName("index.jsp");
	 return mav;
 }
}
