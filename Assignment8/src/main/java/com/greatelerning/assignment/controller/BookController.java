package com.greatelerning.assignment.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.greatelerning.assignment.bean.Book;
import com.greatelerning.assignment.service.BookService;


@Controller
public class BookController {
	@Autowired
	BookService bookService;
	@GetMapping(value="Books")
	public ModelAndView getAllProduct(HttpServletRequest req) {
		ModelAndView mav = new ModelAndView();
		List<Book> listOfBook = bookService.getAllbook(); 
		req.setAttribute("books", listOfBook);
	
		mav.setViewName("Home.jsp");
		return mav;
	}
}
