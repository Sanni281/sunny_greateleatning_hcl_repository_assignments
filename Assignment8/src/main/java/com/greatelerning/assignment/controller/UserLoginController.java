package com.greatelerning.assignment.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.greatelerning.assignment.bean.Book;
import com.greatelerning.assignment.bean.User;
import com.greatelerning.assignment.dao.UserLoginDao;
import com.greatelerning.assignment.service.BookService;

@Controller
public class UserLoginController {
	@Autowired
	UserLoginDao userLoginDao ;
	@Autowired
	BookService bookService ;
	@PostMapping(value="login")
	public ModelAndView userLogin(@RequestParam("email") String email,@RequestParam("password") String password, HttpServletRequest req, HttpSession hs)
	{
//		String e=req.getParameter("email");
//		String p=req.getParameter("password");
	User user = new User();
	ModelAndView mav = new ModelAndView();
	user.setEmail(email);
	user.setPassword(password);
User name= userLoginDao.UserLogin(user)	;
	  if(name!=null)
	  {		
	   List<Book> listOfBook = bookService.getAllbook(); 
		req.setAttribute("books", listOfBook);
		
			hs.setAttribute("obj", email);
			mav.setViewName("AfterLoginIndex.jsp");
return mav;
	}else
	{
		mav.setViewName("Home.jsp");
		return mav;
	}
	
			

	}

}
