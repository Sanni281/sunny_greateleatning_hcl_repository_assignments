package com.greatelerning.assignment.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.greatelerning.assignment.bean.Book;
import com.greatelerning.assignment.bean.User;



@Repository
public class UserLoginDao {
	@Autowired

	JdbcTemplate jdbcTemplate;
	public  User UserLogin(User user) {

	
				return  (User) jdbcTemplate.queryForObject("select * from user where email=? and password=?", new Object[] {user.getEmail(),user.getPassword()}, new UserRowMapper());
	
		}
	class UserRowMapper implements  org.springframework.jdbc.core.RowMapper<User> {
		@Override
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
		User user=new User();
			user.setEmail(rs.getString(3));
			user.setPassword(rs.getString(4));
			return user;
		}
	}
}
