package com.greatelerning.assignment.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.greatelerning.assignment.bean.Book;

@Repository
public class ViewBooksDao {
@Autowired
JdbcTemplate jdbcTemplate;

	public Book getAllBook(int bookid) {
			 String sql="select * from book where bookId=?";
		// return  jdbcTemplate.queryForObject(sql, new Object[]{id}, new BooklikeRowMapper());
return jdbcTemplate.queryForObject(sql, new Object[]{bookid}, new BookLikeRowMapper());

	}

public class BookLikeRowMapper implements RowMapper<Book>
{

	@Override
	public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
		Book book =new Book();
		book.setBookId(rs.getInt(1));
		book.setImage(rs.getString(2));
		book.setBookType(rs.getString(3));
	book.setBookName(rs.getString(4));
	book.setAuthorName(rs.getString(5));
		return book;
	
	}
	
}
}
