package com.greatelerning.assignment.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.greatelerning.assignment.bean.Book;
import com.greatelerning.assignment.dao.ViewBooksDao.BookLikeRowMapper;


@Repository
public class BookDao {
	@Autowired
	JdbcTemplate jdbcTemplate;

	public List<Book> getAllBook() {
		return jdbcTemplate.query("select * from book", new BookRowMapper());
	}


	class BookRowMapper implements  org.springframework.jdbc.core.RowMapper<Book> {
		@Override
		public Book mapRow(ResultSet rs, int rowNum) throws SQLException {
			Book book =new Book();
			book.setBookId(rs.getInt(1));
			book.setImage(rs.getString(2));
			book.setBookType(rs.getString(3));
		book.setBookName(rs.getString(4));
		book.setAuthorName(rs.getString(5));
			return book;
		}
	}
}
