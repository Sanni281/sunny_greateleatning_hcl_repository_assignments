package com.greatelerning.assignment.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.greatelerning.assignment.bean.User;

@Repository
public class UserRegistrationDao {
	@Autowired
	JdbcTemplate jdbcTemplate;

	public int storeUserInfo(User ur) 
	{
		try {
			// same method for insert , delete and update 
			return jdbcTemplate.update("insert into user values(?,?,?,?)",ur.getFirstName(),ur.getLastName(),ur.getEmail(),ur.getPassword());
		} catch (Exception e) {
			System.out.println(e);
			return 0;
		}
	}
}
