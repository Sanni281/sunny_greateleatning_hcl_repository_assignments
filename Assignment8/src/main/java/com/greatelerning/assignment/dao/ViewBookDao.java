package com.greatelerning.assignment.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.greatelerning.assignment.bean.Book;

@Repository
public class ViewBookDao {
@Autowired
JdbcTemplate jdbcTemplate;
public Book retrieveBookInfo( int id) {

	try {
			// we get the connection 
		PreparedStatement pstmt = ((Connection) jdbcTemplate).prepareStatement("select * from Book where bookId=?");
		pstmt.setInt(1, id);
		ResultSet rs = pstmt.executeQuery();
	
		Book book =new Book();
		while(rs.next()) {
	
			book.setBookId(rs.getInt(1));
			book.setImage(rs.getString(2));
			book.setBookType(rs.getString(3));
		book.setBookName(rs.getString(4));
		book.setAuthorName(rs.getString(5));
			return book;
		}
	} catch (Exception e) {
		System.out.println(e);
		
	}
	return null;

}
}
