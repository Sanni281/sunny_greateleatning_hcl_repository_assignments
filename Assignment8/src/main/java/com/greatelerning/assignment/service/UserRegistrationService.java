package com.greatelerning.assignment.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatelerning.assignment.bean.User;
import com.greatelerning.assignment.dao.UserRegistrationDao;

@Service
public class UserRegistrationService {
	@Autowired
	UserRegistrationDao userRegistrationDao;
	public String storeUser(User user) {
		if(user.getEmail().contains("@")) {
		
			if(userRegistrationDao.storeUserInfo(user)>0) {
				return "Record stored successfully";
			}else {
				return "Record didn't store";
			}
		
	}
		else
		{
			return "Invalid Email id";
		}
	}
}
