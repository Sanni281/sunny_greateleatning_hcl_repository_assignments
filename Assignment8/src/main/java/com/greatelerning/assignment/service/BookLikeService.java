package com.greatelerning.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatelerning.assignment.bean.Book;
import com.greatelerning.assignment.dao.ViewBookDao;



@Service
public class BookLikeService {
@Autowired
ViewBookDao viewBookDao;

	public Book getAllBooks(int id){ 
		return viewBookDao.retrieveBookInfo(id);
		
}
}
