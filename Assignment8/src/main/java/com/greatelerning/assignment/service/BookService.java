package com.greatelerning.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatelerning.assignment.bean.Book;
import com.greatelerning.assignment.dao.BookDao;



@Service
public class BookService {
	@Autowired
	BookDao   bookDao;
	
	public List<Book> getAllbook() {
		return bookDao.getAllBook();	}
}
