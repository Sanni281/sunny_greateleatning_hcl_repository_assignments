import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { AdminComponent } from './admin/admin.component';
import { MenuComponent } from './menu/menu.component';
const routes: Routes = [
  {path:"welcome",component:WelcomeComponent},
  {path:"admin",component:AdminComponent},
  {path:"menu",component:MenuComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
