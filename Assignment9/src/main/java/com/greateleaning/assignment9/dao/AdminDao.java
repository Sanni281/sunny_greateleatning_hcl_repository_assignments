package com.greateleaning.assignment9.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.greateleaning.assignment9.bean.Admin;
@Repository
public interface AdminDao extends JpaRepository<Admin, String> {
	
	Admin findByEmailAndPassword(@Param("email")String email, @Param("password")String password);

}
