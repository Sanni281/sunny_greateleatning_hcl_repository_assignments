package com.greateleaning.assignment9.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greateleaning.assignment9.bean.User;
@Repository
public interface UserDao extends JpaRepository<User, String> {

}
