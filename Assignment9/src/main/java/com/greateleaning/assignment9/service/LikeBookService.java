package com.greateleaning.assignment9.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greateleaning.assignment9.bean.Book;
import com.greateleaning.assignment9.bean.Likes;
import com.greateleaning.assignment9.dao.LikeBookDao;

@Service
public class LikeBookService {
@Autowired
LikeBookDao likeBookDao;
public List<Likes> getAllBooks()
{
	return likeBookDao.findAll();
}
}
