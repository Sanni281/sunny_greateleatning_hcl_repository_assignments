package com.greatelearning.bookstore.service;

import java.util.List;

import com.greatelearning.bookstore.bean.Book;
import com.greatelearning.bookstore.dao.BookDao;

public class BookService {

	public List<Book> getAllBooks(){
		BookDao bookdao=new BookDao();
		return bookdao.showBooks();
	}
}
