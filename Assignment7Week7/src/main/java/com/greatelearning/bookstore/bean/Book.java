package com.greatelearning.bookstore.bean;

public class Book {
private int bookId;
private String image;
private String bookType;
private String  bookName;
private String authorName;

public Book() {
	super();
	// TODO Auto-generated constructor stub
}
public Book(int bookId, String image, String bookType, String bookName, String authorName) {
	super();
	this.bookId = bookId;
	this.image = image;
	this.bookType = bookType;
	this.bookName = bookName;
	this.authorName = authorName;
}
public int getBookId() {
	return bookId;
}
public void setBookId(int bookId) {
	this.bookId = bookId;
}
public String getImage() {
	return image;
}
public void setImage(String image) {
	this.image = image;
}
public String getBookType() {
	return bookType;
}
public void setBookType(String bookType) {
	this.bookType = bookType;
}
public String getBookName() {
	return bookName;
}
public void setBookName(String bookName) {
	this.bookName = bookName;
}
public String getAuthorName() {
	return authorName;
}
public void setAuthorName(String authorName) {
	this.authorName = authorName;
}

}
