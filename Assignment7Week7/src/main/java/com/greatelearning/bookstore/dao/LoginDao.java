package com.greatelearning.bookstore.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatelearning.bookstore.bean.Book;
import com.greatelearning.bookstore.bean.User;
import com.greatelearning.bookstore.resource.DbResource;

public class LoginDao {
	public static boolean login(String email,String password) {
		boolean status=false;
		try {
			
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from user where email=? and password=?");
			pstmt.setString(1,email);
			pstmt.setString(2, password);
			
			ResultSet rs=pstmt.executeQuery();
		
			status=rs.next();
		} catch (Exception e) {
			System.out.println(" Something Wrong"+e);
			return status;
		}
		return status;
	}
}
