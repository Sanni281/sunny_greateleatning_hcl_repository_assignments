package com.greatelearning.bookstore.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;

import com.greatelearning.bookstore.bean.User;
import com.greatelearning.bookstore.resource.DbResource;

public class UserDao {
		public int storeEmployee(User user) {
			try {
				Connection con = DbResource.getDbConnection();
				PreparedStatement pstmt = con.prepareStatement("insert into user values(?,?,?,?)");
				pstmt.setString(1, user.getFirstName());
				pstmt.setString(2, user.getLastName());
				pstmt.setString(3, user.getEmail());
				pstmt.setString(4, user.getPassword());
				return pstmt.executeUpdate();
			} catch (Exception e) {
				System.out.println("Store Method Exception "+e);
				return 0;
			}
		}

}
