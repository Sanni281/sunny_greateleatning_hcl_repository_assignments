package com.greatelearning.bookstore.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatelearning.bookstore.bean.Book;
import com.greatelearning.bookstore.bean.User;
import com.greatelearning.bookstore.resource.DbResource;

public class BookDao {

	
	public List<Book> showBooks() {
		List<Book> listOfBook=new ArrayList<Book>();
		try {
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from book");
			ResultSet rs=pstmt.executeQuery();
			while(rs.next())
			{
				Book book=new Book();
				book.setBookId(rs.getInt(1));
				book.setImage(rs.getString(2));
				book.setBookType(rs.getString(3));
				book.setBookName(rs.getString(4));
				book.setAuthorName(rs.getString(5));
                listOfBook.add(book);

				
			}
			return listOfBook;
		} catch (Exception e) {
			System.out.println(" Something Wrong"+e);
			return null;
		}
	}
}
