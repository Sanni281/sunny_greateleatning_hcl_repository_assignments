package com.greatelearning.bookstore.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatelearning.bookstore.bean.Book;
import com.greatelearning.bookstore.resource.DbResource;

public class ViewBooksDao {
	public Book ViewBooks(int id) {
		List<Book> listOfBook=new ArrayList<Book>();
		try {
			Connection con = DbResource.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from book where bookId=?");
			pstmt.setInt(1, id);
			ResultSet rs=pstmt.executeQuery();
			Book book=new Book();
			while(rs.next())
			{
			
				book.setBookId(1);
				book.setImage(rs.getString(2));
				book.setBookType(rs.getString(3));
				book.setBookName(rs.getString(4));
				book.setAuthorName(rs.getString(5));
                

				
			}
			return book;
		} catch (Exception e) {
			System.out.println(" Something Wrong"+e);
			return null;
		}
	}
}
