package com.greatelearning.assignment.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.web.client.RestTemplate;

import com.greatelearning.assignment.bean.User;
import com.greatelearning.assignment.dao.UserDao;
import com.greatelearning.assignment.service.UserService;
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
class UserControllerTest {
	@Autowired
	UserDao userDao;
	String baseUrl="http://localhost:8185/admin/users";
	@Test
//	@Disabled
	void testInsertUser() {
		//fail("Not yet implemented");
		RestTemplate restTemplate= new RestTemplate();
		User user=new User();
		user.setEmail("deepal@gmail.com");
		user.setFirstname("deppak");
		user.setLastname("pal");
		user.setPassword("123");
		String res=restTemplate.postForObject(baseUrl+"/addUser", user,String.class);
	
	}

	@Test
	@Disabled
	void testGetAllUserInfo() {
		//fail("Not yet implemented");
		
		RestTemplate restTemplate= new RestTemplate();
		User user = new User();
		restTemplate.getForObject(baseUrl+"/getUsers", user.getClass());
		
	}

	@Test
	//@Disabled
	void testDeleteData() {
		//fail("Not yet implemented");
		RestTemplate restTemplate= new RestTemplate();
	restTemplate.delete("http://localhost:8185/admin/users/deleteuser/cxc@gfg",User.class);
			
	}

	@Test
	@Disabled
	void testUpdateDetails() {
		//fail("Not yet implemented");
		RestTemplate restTemplate= new RestTemplate();
		User user=new User();
		user.setEmail("sanni@gmail.com");
		user.setFirstname("deppak");
		user.setLastname("rock");
	
		restTemplate.patchForObject(baseUrl+"/userUpdate", user,String.class);
	
	}

}
