package com.greatelearning.assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatelearning.assignment.bean.User;
import com.greatelearning.assignment.service.UserService;


@RestController
@RequestMapping("admin/users")
public class UserController {
@Autowired
UserService userService;
@PostMapping(value="addUser", consumes = MediaType.APPLICATION_JSON_VALUE)
public String insertUser(@RequestBody User user)
{
	return  userService.addUserINfo(user);
}
@GetMapping(value="getUsers",produces = MediaType.APPLICATION_JSON_VALUE)
public List<User> getAllUserInfo()
{
	return userService.getAllUsers();
}
@DeleteMapping(value = "deleteuser/{email}" )
public String deleteData(@PathVariable("email")  String email)
{
	return userService.deleteUsers(email);
}
@PatchMapping(value="userUpdate",
consumes = MediaType.APPLICATION_JSON_VALUE,
produces= MediaType.TEXT_PLAIN_VALUE)
public String updateDetails(@RequestBody User user)	
{
return userService.updateUser(user);
}

}
