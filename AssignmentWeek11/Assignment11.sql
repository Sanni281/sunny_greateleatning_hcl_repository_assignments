create Databse sanni_books;
use sanni_books;

create table user(firstname varchar(30),lastname varchar(30),email varchar(40) primary key,password varchar(50));

insert into user values('sunny','chourasiya','sunny@gmail.com','s123');
insert into user values('ajay','verma','ajay@gmail.com','a123');
insert into user values('Deepak','pal','deepak@gmail.com','d123');


create table book(bookid int primary key,image varchar(400),booktype varchar(30),bookname varchar(40),authorname varchar(50));

insert into book values(1,'https://images-eu.ssl-images-amazon.com/images/I/51vgy3LMz6L._SY264_BO1,204,203,200_QL40_FMwebp_.jpg','Autobiography','Wings of fire','A. P. J. Abdul Kalam ');

insert into book values(2,'https://images-na.ssl-images-amazon.com/images/I/41YARni+LPL._SX460_BO1,204,203,200_.jpg','Novel','One Day, Life Will Change','Saranya Umakanthan');

insert into book values(3,'https://images-eu.ssl-images-amazon.com/images/I/51ckaBrXWmL._SX198_BO1,204,203,200_QL40_FMwebp_.jpg','Novel','Shadow of the Past','Mayank Manohar');

insert into book values(4,'https://m.media-amazon.com/images/I/51LPnhpRjYS._SX260_.jpg','Kindle','Animals Pictures','BlueGorilla Press');
insert into book values(5,'https://m.media-amazon.com/images/I/41EpJgi-D7L.jpg','Action','Price Action Trading Secrets','Rayner Teo');
insert into book values(6,'https://m.media-amazon.com/images/I/41BS2QN9bCL.jpg','Drama','How to See a Breakout','SUDHIR DIXIT');
insert into book values(7,'https://images-na.ssl-images-amazon.com/images/I/51arVE3ZyoS._SX460_BO1,204,203,200_.jpg','Biographi','My Inventions, Autobiography','Nikola Tesla ');


create table admin(firstname varchar(30),lastname varchar(30),email varchar(40) primary key,password varchar(50));
insert into admin values('hritik','pratap','hritik@gmail.com','h123');
insert into admin values('canny','arora','canny@gmail.com','c123');

create table likes(bookid int primary key,image varchar(400),booktype varchar(30),bookname varchar(40),authorname varchar(50));


insert into likes values(4,'https://m.media-amazon.com/images/I/51LPnhpRjYS._SX260_.jpg','Kindle','Animals Pictures','BlueGorilla Press');
insert into likes values(5,'https://m.media-amazon.com/images/I/41EpJgi-D7L.jpg','Action','Price Action Trading Secrets','Rayner Teo');
insert into likes values(6,'https://m.media-amazon.com/images/I/41BS2QN9bCL.jpg','Drama','How to See a Breakout','SUDHIR DIXIT');
insert into likes values(7,'https://images-na.ssl-images-amazon.com/images/I/51arVE3ZyoS._SX460_BO1,204,203,200_.jpg','Biographi','My Inventions, Autobiography','Nikola Tesla ');


