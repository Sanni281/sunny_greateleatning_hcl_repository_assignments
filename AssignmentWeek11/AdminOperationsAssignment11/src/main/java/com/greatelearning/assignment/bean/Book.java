package com.greatelearning.assignment.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Book {
	@Id
private int bookid;
	private String image;
	private String booktype;
	private String bookname;
	private String authorname;
	
	public Book() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Book(int bookid, String image, String booktype, String bookname, String authorname) {
		super();
		this.bookid = bookid;
		this.image = image;
		this.booktype = booktype;
		this.bookname = bookname;
		this.authorname = authorname;
	}
	public int getBookid() {
		return bookid;
	}
	public void setBookid(int bookid) {
		this.bookid = bookid;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getBooktype() {
		return booktype;
	}
	public void setBooktype(String booktype) {
		this.booktype = booktype;
	}
	public String getBookname() {
		return bookname;
	}
	public void setBookname(String bookname) {
		this.bookname = bookname;
	}
	public String getAuthorname() {
		return authorname;
	}
	public void setAuthorname(String authorname) {
		this.authorname = authorname;
	}
	@Override
	public String toString() {
		return "Book [bookid=" + bookid + ", image=" + image + ", booktype=" + booktype + ", bookname=" + bookname
				+ ", authorname=" + authorname + "]";
	}
	

}
