package com.greatelearning.assignment.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatelearning.assignment.bean.Likes;


@Repository
public interface LikeBookDao extends JpaRepository<Likes, Integer> {

}
