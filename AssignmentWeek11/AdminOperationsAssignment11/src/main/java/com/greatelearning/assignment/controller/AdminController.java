package com.greatelearning.assignment.controller;

import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatelearning.assignment.bean.Admin;
import com.greatelearning.assignment.bean.Book;
import com.greatelearning.assignment.service.AdminService;
import com.greatelearning.assignment.service.BookService;


@RestController
@RequestMapping("/admin")
@EntityScan(basePackages = "com.greateleaning.assignment9.bean")
public class AdminController {
@Autowired
AdminService adminService;
@Autowired
BookService bookService;

@PostMapping(value="addAdmin", consumes = MediaType.APPLICATION_JSON_VALUE)
public String insertAdmin(@RequestBody Admin admin)
{
	return  adminService.addAdminINfo(admin);
}



@PostMapping(value="login",
consumes = MediaType.APPLICATION_JSON_VALUE,
produces= MediaType.TEXT_PLAIN_VALUE)
public String adminDetails(@RequestBody Admin admin)	
{
Admin find= adminService.login(admin.getEmail(), admin.getPassword());
	
	if(Objects.nonNull(find))	
	{
		
		return "login Successfully";
	}
	else
	{
		
		return "login failed"; 
	}
}
@PostMapping(value="logout",
consumes = MediaType.APPLICATION_JSON_VALUE,
produces= MediaType.TEXT_PLAIN_VALUE)
public String adminDetails(HttpSession hs)
{
	 hs.invalidate();
	return "logout Sussefully";
}
@PostMapping(value="addbooks", consumes = MediaType.APPLICATION_JSON_VALUE)
public String insertBooks(@RequestBody Book book)
{
	return bookService.addBookINfo(book);
}
@GetMapping(value="getBooks",produces = MediaType.APPLICATION_JSON_VALUE)
public List<Book> getAllBookInfo()
{
	return bookService.getAllBooks();
}
@DeleteMapping(value = "delete/{bookid}" )
public String deleteData(@PathVariable("bookid")  int bookid)
{
	return bookService.deleteBook(bookid);
}

@PatchMapping(value="bookupdate",
consumes = MediaType.APPLICATION_JSON_VALUE,
produces= MediaType.TEXT_PLAIN_VALUE)
public String updateDetails(@RequestBody Book book)	
{
return bookService.updateBook(book);
}
}
