package com.greatelearning.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatelearning.assignment.bean.User;
import com.greatelearning.assignment.dao.UserDao;


@Service
public class UserService {
@Autowired
UserDao userDao;
public String addUserINfo(User user)
{
	if(userDao.existsById(user.getEmail()))
	{
		return "this email id already present present";
	}
	else
	{
		userDao.save(user);
		return " User Information store";
	}
}
public List<User> getAllUsers()
{
	return userDao.findAll();
}


public String deleteUsers(String email)
{
	System.out.println(email);
	if(!userDao.existsById(email))
	{
		return "id  not present";
	}
	else
	{
	userDao.deleteById(email);
		return "deleted";
	}

}


public String updateUser(User user)
{
	if(!userDao.existsById(user.getEmail()))
	{
		return "User Id not present";
	}
	else
	{
		User u= userDao.getById(user.getEmail());
		u.setFirstname(user.getFirstname());
		
u.setLastname(user.getLastname());
u.setEmail(user.getEmail());
u.setPassword(user.getPassword());
  userDao.saveAndFlush(u);
		return " User Details updated Successfully";
	}	
}
}
