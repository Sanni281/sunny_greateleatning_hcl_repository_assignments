package com.greatelearning.assignment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatelearning.assignment.bean.Likes;
import com.greatelearning.assignment.dao.LikeBookDao;


@Service
public class LikeBookService {
@Autowired
LikeBookDao likeBookDao;
public List<Likes> getAllBooks()
{
	return likeBookDao.findAll();
}
}
