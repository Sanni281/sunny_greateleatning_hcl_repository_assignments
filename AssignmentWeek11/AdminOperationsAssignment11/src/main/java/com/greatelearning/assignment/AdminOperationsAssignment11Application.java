package com.greatelearning.assignment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class AdminOperationsAssignment11Application {

	public static void main(String[] args) {
		SpringApplication.run(AdminOperationsAssignment11Application.class, args);
	}
}
