package com.greatelearning.miniproject.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatelearning.miniproject.bean.Users;
import com.greatelearning.miniproject.dao.UserLoginDao;
import com.greatelearning.miniproject.dao.UserRegistrationDao;

@Service
public class UserRegistrationService {
	@Autowired
	UserRegistrationDao userRegistrationDao;
	public String storeUser(Users user) {
			if(userRegistrationDao.existsById(user.getEmail()))
			{
				return "id  not present";
			}
			else
			{
				userRegistrationDao.save(user);
				return " store";
			}	
	}
	public List<Users> getAllUsers()
	{
		return userRegistrationDao.findAll();
	}
	
	
	public Users updateUser(String email,Users user)
	{
		System.out.println(email);
	     Users u=userRegistrationDao.findById(email).orElse(null);
	     u.setFname(user.getFname());
	     u.setLname(user.getLname());
         u.setPassword(user.getPassword());
	return   userRegistrationDao.saveAndFlush(u);
	      
		}	
	
	public String deleteuser(String email)
	{
		if(!userRegistrationDao.existsById(email))
		{
			return "id not present";
		}
		else
		{
		    userRegistrationDao.deleteById(email);
			return " User information deleted";
		}
	}
}
