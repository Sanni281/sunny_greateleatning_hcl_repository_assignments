package com.greatelearning.miniproject.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatelearning.miniproject.bean.Menu;
import com.greatelearning.miniproject.bean.Users;
import com.greatelearning.miniproject.dao.MenuDao;

@Service
public class MenuService {
@Autowired
MenuDao menuDao;
public List<Menu> getAllMenu()
{
	return menuDao.findAll();
}

public String storeItems(Menu menu) {
	if(menuDao.existsById(menu.getMenuname()))
	{
		return "id  not present";
	}
	else
	{
		menuDao.save(menu);
		return " store";
	}	
}



public String deleteBook(String menuname)
{
	if(!menuDao.existsById(menuname))
	{
		return "id  not present";
	}
	else
	{
	menuDao.deleteById(menuname);;
		return "deleted";
	}

}


public Menu updateites(String menuname,Menu menu)
{
	
	
	
  Menu m= menuDao.findById(menuname).orElse(null);
 
  m.setDescription(menu.getDescription());
  m.setImage(menu.getImage());
  m.setPrice(menu.getPrice());
 return  menuDao.saveAndFlush(m);
         
	
}

}
