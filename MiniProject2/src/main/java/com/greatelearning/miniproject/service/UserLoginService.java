package com.greatelearning.miniproject.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatelearning.miniproject.bean.Users;
import com.greatelearning.miniproject.dao.UserLoginDao;

@Service
public class UserLoginService {
@Autowired
UserLoginDao userDao;
public Users login(String email,String password)
{
	Users user=userDao.findByEmailAndPassword(email, password);
	return user;
}
public Users updateuser(String email,Users users)
{
	System.out.println(email);
     Users u=userDao.findById(email).orElse(null);
     u.setFname(users.getFname());
     u.setLname(users.getLname());
     u.setPassword(users.getPassword());
return   userDao.saveAndFlush(u);
      
	}	

public String deleteuser(String email)
{
	if(!userDao.existsById(email))
	{
		return "id not present";
	}
	else
	{
	    userDao.deleteById(email);
		return " User information deleted";
	}
}
}
