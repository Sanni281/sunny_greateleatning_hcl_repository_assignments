package com.greatelearning.miniproject.dao;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import com.greatelearning.miniproject.bean.Bills;

public interface BillsDao extends JpaRepository<Bills, Integer> {
	//@Query("select b from Bills b where DATE(b.ordertime)=DATE(NOW()+INTERVAL 0 DAY) AND b.email=:email")
	public List<Bills>  getBillsByEmail(@Param("email") String email);
	
	public List<Bills>  getBillsByOrdertime(@Param("ordertime") Date ordertime);
	
	public List<Bills>  getBillsByOrdertimeAndEmail(@Param("ordertime") Date ordertime, @Param("email") String Email);

}
