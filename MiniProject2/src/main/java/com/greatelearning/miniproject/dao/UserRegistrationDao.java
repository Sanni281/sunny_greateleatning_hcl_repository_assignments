package com.greatelearning.miniproject.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.greatelearning.miniproject.bean.Users;

public interface UserRegistrationDao extends JpaRepository<Users, String> {
}
