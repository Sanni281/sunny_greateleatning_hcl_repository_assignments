package com.greatelearning.miniproject.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestMapping;

import com.greatelearning.miniproject.bean.Admin;
@RequestMapping
public interface AdminDao extends JpaRepository<Admin, String> {
	Admin findByEmailAndPassword(@Param("email")String email, @Param("password")String password);

}
