package com.greatelearning.miniproject.dao;

import java.util.List;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.greatelearning.miniproject.bean.Bill;
import com.greatelearning.miniproject.bean.Users;
@Repository
public interface BillDao extends JpaRepository<Bill, Integer> {
	@Query("SELECT b from Bill b where b.email =: email")
	public List<Bill> findByEmail(@Param("email") String email);

}
