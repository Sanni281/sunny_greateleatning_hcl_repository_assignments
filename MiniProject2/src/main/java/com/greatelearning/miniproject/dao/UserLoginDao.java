package com.greatelearning.miniproject.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.greatelearning.miniproject.bean.Users;

@Repository
public interface UserLoginDao extends JpaRepository<Users, String> {
Users findByEmailAndPassword(@Param("email")String email, @Param("password")String password);

}
