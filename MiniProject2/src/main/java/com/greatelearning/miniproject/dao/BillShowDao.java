package com.greatelearning.miniproject.dao;

import java.util.List;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.greatelearning.miniproject.bean.Bill;
@Repository
public interface BillShowDao extends JpaRepository<Bill, Integer> {
	//@Query("select bi from Bill bi where bi.email =: email")
	public List<Bill>  getBillByEmail(@Param("email") String email);

}
