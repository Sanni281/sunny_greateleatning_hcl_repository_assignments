package com.greatelearning.miniproject.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.greatelearning.miniproject.bean.Menu;
@Repository
public interface MenuDao extends JpaRepository<Menu, String> 
{
	
}
