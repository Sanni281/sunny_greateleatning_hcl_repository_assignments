package com.greatelearning.miniproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "com")
public class MiniProject2Application {

	public static void main(String[] args) {
		SpringApplication.run(MiniProject2Application.class, args);
	}

}
