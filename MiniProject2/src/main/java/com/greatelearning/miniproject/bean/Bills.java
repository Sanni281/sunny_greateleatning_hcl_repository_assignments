package com.greatelearning.miniproject.bean;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Bills {
	@Id
private int id;
	private String menuname;
	private int price;
	private String email;
	//@DateTimeFormat(pattern = "yyyy-MM-dd")

	private Date ordertime;

public Bills() {
		super();
		// TODO Auto-generated constructor stub
	}
public Bills(int id, String menuname, int price, String email, Date ordertime) {
		super();
		this.id = id;
		this.menuname = menuname;
		this.price = price;
		this.email = email;
		this.ordertime = ordertime;
	}
public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getMenuname() {
		return menuname;
	}
	public void setMenuname(String menuname) {
		this.menuname = menuname;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getOrdertime() {
		return ordertime;
	}
	public void setOrdertime(Date ordertime) {
		this.ordertime = ordertime;
	}
	@Override
	public String toString() {
		return "Bills [id=" + id + ", menuname=" + menuname + ", price=" + price + ", email=" + email + ", ordertime="
				+ ordertime + "]";
	}

}
