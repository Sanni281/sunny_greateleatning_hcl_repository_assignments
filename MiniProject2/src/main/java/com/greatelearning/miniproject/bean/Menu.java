package com.greatelearning.miniproject.bean;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Menu {
	@Id
	private String menuname;
	private String  description;
	private String image;
	private int price;
	
	public Menu() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Menu(String menuname, String description, String image, int price) {
		super();
		this.menuname = menuname;
		this.description = description;
		this.image = image;
		this.price = price;
	}
	public String getMenuname() {
		return menuname;
	}
	public void setMenuname(String menuname) {
		this.menuname = menuname;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		return "Menu [menuname=" + menuname + ", description=" + description + ", image=" + image + ", price=" + price
				+ "]";
	}
	

}
