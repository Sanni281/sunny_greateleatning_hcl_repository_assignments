package com.greatelearning.miniproject.bean;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.data.relational.core.mapping.Table;

@Entity()
@javax.persistence.Table(name="bill")
public class Bill {
	@Id
	@Column(name="id")
private int id;
private String menuname;
private int price;
private String email;

public Bill() {
	super();
	
}
public Bill(int id, String menuname, int price, String email) {
	super();
	this.id = id;
	this.menuname = menuname;
	this.price = price;
	this.email = email;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getMenuname() {
	return menuname;
}
public void setMenuname(String menuname) {
	this.menuname = menuname;
}
public int getPrice() {
	return price;
}
public void setPrice(int price) {
	this.price = price;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
@Override
public String toString() {
	return "bill [id=" + id + ", menuname=" + menuname + ", price=" + price + ", email=" + email + "]";
}

}
