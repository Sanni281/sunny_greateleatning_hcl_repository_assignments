package com.greatelearning.miniproject.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.annotation.JsonCreator.Mode;
import com.greatelearning.miniproject.bean.Menu;
import com.greatelearning.miniproject.bean.Users;
import com.greatelearning.miniproject.dao.MenuDao;
import com.greatelearning.miniproject.service.MenuService;


@Controller
public class MenuController {
@Autowired
MenuService menuService;
@Autowired
MenuDao menuDao;
@GetMapping(value="display")
public String showAll(HttpSession hs)
{
	List<Menu> listOf=menuService.getAllMenu();
	hs.setAttribute("items", listOf);
	return "UserMenuShow.jsp";
}


@GetMapping(value="displaymenu1")
public ModelAndView showAllmenu(HttpSession hs)
{
	System.out.println("hiffgd");
	ModelAndView mav=new ModelAndView();
	List<Menu> list=menuService.getAllMenu();
	hs.setAttribute("item", list);
	mav.setViewName("AdminMenuShow.jsp");
	return mav;
}
@GetMapping(value="menudelete/{menuname}")
public String Delete(@PathVariable("menuname") String e) {
ModelAndView mav= new ModelAndView();
menuService.deleteBook(e);
System.out.println("hhh");
return "redirect:/MenuPanel.jsp";

}


@PostMapping(value="addmenu")
public ModelAndView userRegistration(@ModelAttribute Menu menu)
{
	ModelAndView mav= new ModelAndView();
	
menuService.storeItems(menu);
	 mav.setViewName("AdminMenuShow.jsp");
	 return mav;

}



@RequestMapping(value="updateites/{menuname}",method = RequestMethod.GET)
public ModelAndView finda(@PathVariable("menuname") String menuname,  Model model,HttpSession hs)
{
	System.out.println("hhhhhh");
	ModelAndView mav= new ModelAndView();
model.addAttribute("menuname",menuname);
model.addAttribute("comand",menuDao.findById(menuname) );
	mav.setViewName("redirect:/updateitems.jsp");
	hs.setAttribute("ob2", menuname);
	System.out.println("ddjdfl");
     return mav;


}

@PostMapping(value="updateites/{menuname}")
public ModelAndView showAl(@PathVariable("menuname") String menuname,@ModelAttribute Menu menu,   HttpSession hs)
{   System.out.println(menuname);
	ModelAndView mav= new ModelAndView();
    menuService.updateites(menuname, menu);
mav.setViewName("redirect:/MenuPanel.jsp");
	return mav;	
} 



}
