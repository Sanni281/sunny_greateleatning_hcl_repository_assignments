package com.greatelearning.miniproject.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.greatelearning.miniproject.bean.Menu;
import com.greatelearning.miniproject.bean.Users;
import com.greatelearning.miniproject.dao.UserRegistrationDao;
import com.greatelearning.miniproject.service.UserLoginService;
import com.greatelearning.miniproject.service.UserRegistrationService;

@Controller
public class UserRegistrationController {
	@Autowired
	UserRegistrationService userRegistrationService;
@Autowired
UserRegistrationDao userRegistrationDao;
@Autowired 
UserLoginService userLoginService;


	@PostMapping(value="registration")
	public ModelAndView userRegistration(@ModelAttribute Users user)
	{
		ModelAndView mav= new ModelAndView();
		
		userRegistrationService.storeUser(user);
		 mav.setViewName("index.jsp");
		 return mav;

}
	@GetMapping(value="userdisplay")
	public String showAll(HttpSession hs)
	{
		List<Users> listOf=userRegistrationService.getAllUsers();
		hs.setAttribute("users", listOf);
		return "showUserDetails.jsp";
	}
	
	
	
	@RequestMapping(value="updateUser/{email}",method = RequestMethod.GET)
	public ModelAndView finda(@PathVariable("email") String email,  Model model,HttpSession hs)
	{
		System.out.println("hhhhhh");
		ModelAndView mav= new ModelAndView();
	model.addAttribute("email",email);
model.addAttribute("comand",userRegistrationDao.findById(email) );
		mav.setViewName("redirect:/updateUserDetails.jsp");
		hs.setAttribute("ob", email);
		System.out.println("ddjdfl");
         return mav;
	
	
	}
	
	@PostMapping(value="updateUser/{email}")
	public ModelAndView showAl(@PathVariable("email") String email,@ModelAttribute Users user,   HttpSession hs)
	{  // System.out.println(user);
		ModelAndView mav= new ModelAndView();
	    userLoginService.updateuser(email,user);
	mav.setViewName("redirect:/MenuPanel.jsp");
		return mav;	
	} 
	
	@GetMapping(value="delete/{email}")
	public String Delete(@PathVariable("email") String e) {
	ModelAndView mav= new ModelAndView();
    userRegistrationService.deleteuser(e);
	System.out.println("hhh");
	return "redirect:/UserPanel.jsp";

	}
	
	
	
	
	@PostMapping(value="registrationByAdmin")
	public ModelAndView userAdd(@ModelAttribute Users user)
	{
		ModelAndView mav= new ModelAndView();
		
		userRegistrationService.storeUser(user);
		 mav.setViewName("UserPanel.jsp");
		 return mav;

}
	
	
}