package com.greatelearning.miniproject.controller;

import java.sql.Date;
import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.greatelearning.miniproject.bean.Bill;
import com.greatelearning.miniproject.bean.Bills;
import com.greatelearning.miniproject.bean.Menu;
import com.greatelearning.miniproject.bean.Users;
import com.greatelearning.miniproject.dao.BillDao;
import com.greatelearning.miniproject.dao.BillShowDao;
import com.greatelearning.miniproject.service.BillService;
import com.greatelearning.miniproject.service.BillShowService;

@Controller
public class BillController {
@Autowired
BillService billService;
@Autowired
BillDao billDao;
@Autowired
BillShowService billShowService;
@PostMapping(value="addbill")
public ModelAndView userRegistration(@ModelAttribute Bills bill)
{
	ModelAndView mav= new ModelAndView();
	
billService.storebill(bill);
	 mav.setViewName("menuforuser.jsp");
	 return mav;

}
//@GetMapping(value="showbill/{email}")
//public ModelAndView showAll(@PathVariable(name="email") String email, HttpSession hs,Bill bill)
//{	ModelAndView mav= new ModelAndView();
//
//		
//	List<Bill> listOf=billService.getMenu(email);
//	System.out.println(email+"fdggfgfgh");
//	hs.setAttribute("bills2", listOf);
//	mav.setViewName("redirect:/userbills.jsp");
//	return mav;
//}

@GetMapping(value="billshow/{email}")
public ModelAndView showbill(@PathVariable("email") String email, HttpSession hs)
{
	
	List<Bills> listOf=billShowService.findBill(email);
	hs.setAttribute("bill4", listOf);
     ModelAndView mav=new ModelAndView();
       mav.setViewName("redirect:/userbills.jsp");
       return mav;

}

@GetMapping(value="billByOrdertimeAndEmail")
public ModelAndView showbillByOrder(@RequestParam("email") String email,@RequestParam("ordertime") Date ordertime   , HttpSession hs)
{
	
	List<Bills> listOf=billShowService.findBillByEmailAndOrdertime(ordertime, email);
	hs.setAttribute("billorder", listOf);

     ModelAndView mav=new ModelAndView();
       mav.setViewName("redirect:showbillOrderandEmail.jsp");
       return mav;

}
}
