package com.greatelearning.miniproject.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
@Controller
public class main {
	@GetMapping(value="index")
	public ModelAndView openIndexPage()
	{
		ModelAndView mav= new ModelAndView();
		mav.setViewName("index.jsp");
		return mav;
	}
}
