package com.greatelearning.miniproject.controller;

import java.sql.Date;
import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.greatelearning.miniproject.bean.Admin;
import com.greatelearning.miniproject.bean.Bills;
import com.greatelearning.miniproject.bean.Users;
import com.greatelearning.miniproject.service.AdminService;
import com.greatelearning.miniproject.service.BillShowService;

@Controller
public class AdminController {
	@Autowired
	AdminService adminService;
	@Autowired
	BillShowService billShowService;
	@PostMapping(value="Adminlogin")
	public ModelAndView login(Admin admin,HttpSession hs,@RequestParam("email") String email)
	{
		System.out.println("hello");
		Admin find= adminService.login(admin.getEmail(), admin.getPassword());
		
		if(Objects.nonNull(find))	
		{
			ModelAndView mav= new ModelAndView();
			hs.setAttribute("obj", email);
			mav.setViewName("AdminDaseBoard.jsp");
			return mav;
		}
		else
		{
			ModelAndView mav= new ModelAndView();
			mav.setViewName("loginfailbyadmin.jsp");	
			return mav; 
		}
	}
	@PostMapping(value="adminRegistration")
	public ModelAndView userRegistration(@ModelAttribute Admin ad)
	{
		ModelAndView mav= new ModelAndView();
		
		adminService.addAdminINfo(ad);
		 mav.setViewName("index.jsp");
		 return mav;

}
	
	@GetMapping(value="billAdmin")
	public ModelAndView showbill(HttpSession hs)
	{
		
		List<Bills> listOf=billShowService.list();
		hs.setAttribute("bill7", listOf);
	     ModelAndView mav=new ModelAndView();
	       mav.setViewName("redirect:/AllBills.jsp");
	       return mav;

	}
	@GetMapping(value="billshowbyid")
	public ModelAndView showbillbyid(@RequestParam("email") String email, HttpSession hs)
	{
		
		List<Bills> listOf=billShowService.findBill(email);
		hs.setAttribute("bill4", listOf);
	     ModelAndView mav=new ModelAndView();
	       mav.setViewName("redirect:/userbills.jsp");
	       return mav;

	}
	@GetMapping(value="billshowbydate")
	public ModelAndView showbillbydate(@RequestParam("ordertime") Date ordertime, HttpSession hs)
	{
		
		List<Bills> listOf=billShowService.findbydate(ordertime);
		hs.setAttribute("bill6", listOf);
	     ModelAndView mav=new ModelAndView();
	       mav.setViewName("redirect:/ShowDateBills.jsp");
	       return mav;

	}
}
