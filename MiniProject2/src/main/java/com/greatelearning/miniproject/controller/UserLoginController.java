package com.greatelearning.miniproject.controller;

import java.nio.file.attribute.UserPrincipalLookupService;
import java.util.List;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.greatelearning.miniproject.bean.Menu;
import com.greatelearning.miniproject.bean.Users;
import com.greatelearning.miniproject.dao.UserRegistrationDao;
import com.greatelearning.miniproject.service.MenuService;
import com.greatelearning.miniproject.service.UserLoginService;
import com.greatelearning.miniproject.service.UserRegistrationService;

@Controller
public class UserLoginController {
@Autowired
UserLoginService userLoginService;
@Autowired
MenuService menuService;
@PostMapping(value="login")
public ModelAndView login(Users user,HttpSession hs,@RequestParam("email") String email)
{

	Users find= userLoginService.login(user.getEmail(), user.getPassword());
	
	if(Objects.nonNull(find))	
	{
		List<Menu> listOf=menuService.getAllMenu();
		hs.setAttribute("items", listOf);
		ModelAndView mav= new ModelAndView();
		hs.setAttribute("obj", email);
		mav.setViewName("menuforuser.jsp");
		return mav;
	}
	else
	{
		ModelAndView mav= new ModelAndView();
		mav.setViewName("registration.jsp");	
		return mav; 
	}
}
}

