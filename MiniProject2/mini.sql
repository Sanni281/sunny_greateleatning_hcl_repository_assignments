create database sanni_restaurant;
use sanni_restaurant;



create table admin(fname varchar(30),lname varchar(30),email varchar(40) primary key,password varchar(50));

insert into admin values('hritik','pratap','hritik@gmail.com','h123');
insert into admin values('canny','arora','canny@gmail.com','c123');

create table users(fname varchar(30),lname varchar(30),email varchar(40) primary key,password varchar(50));

insert into user values('sunny','chourasiya','sunny@gmail.com','s123');
insert into user values('ajay','verma','ajay@gmail.com','a123');
insert into user values('Deepak','pal','deepak@gmail.com','d123');

create table menu(menuname varchar(100) primary key,image varchar(400),description varchar(400),price int);

insert into menu values('Chicken pot pie',' https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTVvmvda0cjAPlBhM_mUBlukpGHQUEYirg_HA&usqp=CAU',' Indian Appetizers on a plate next to small bowls of dipping sauces ? Bhajji',200);

insert into menu values('Lasagna','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ9qZIr-_0LwnXC2ZjUKSipUBOGY93xiD7aTA&usqp=CAU',' Indian Appetizers on a plate next to small bowls of dipping sauces ? Bhajji:',150);

insert into menu values('Mashed potatoes','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT5s-ELVTqxpnfp-jKt7ZnczirmBa_8QpsJ6Q&usqp=CAU','Creamy Make Ahead Mashed Potato in a white bowl, ready for serving. Whenever you order a dish with mashed potatoes at a restaurant',120);

insert into menu values('Spaghetti with meatballs','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTFNdQ2PAC97MMhf6-r78SvRRCNvu5LNToEtw&usqp=CAU','Spaghetti with meatballs | spaghetti with meatballs is an Italian and Italian-American dish consisting of spaghetti, tomato sauce and meatballs.combinations of pasta',180);

create table bills(id in primary key auto_increment,menuname varchar(100),price int,ordertime timestamp,email varchar(400));