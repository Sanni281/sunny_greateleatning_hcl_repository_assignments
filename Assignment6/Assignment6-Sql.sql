create database movies_sanni_chaurasiya;
use movies_sanni_chaurasiya;

create table moviescoming(id int primary key,title varchar(20),year varchar(20),genres varchar(70),rating varchar(100),poster varchar(200),releaseDate varchar(20),averageRating int,storyline varchar(200),actors varchar(80),posterurl varchar(150));

create table moviesintheaters(id int primary key,title varchar(20),year varchar(20),genres varchar(70),rating varchar(100),poster varchar(200),releaseDate varchar(20),averageRating int,storyline varchar(200),actors varchar(80),posterurl varchar(150));

create table topratedindia(id int primary key,title varchar(20),year varchar(20),genres varchar(70),rating varchar(100),poster varchar(200),releaseDate varchar(20),averageRating int,storyline varchar(200),actors varchar(80),posterurl varchar(150));

 create table topratedmovies(id int primary key,title varchar(20),year varchar(20),genres varchar(70),rating varchar(100),poster varchar(200),releaseDate varchar(20),averageRating int,storyline varchar(200),actors varchar(80),posterurl varchar(150));

 create table favourite(id int primary key,title varchar(20),year varchar(20),genres varchar(70),rating varchar(100),poster varchar(200),releaseDate varchar(20),averageRating int,storyline varchar(200),actors varchar(80),posterurl varchar(150));

//Movies Coming data Insert

insert into moviescoming values(1,"Game Night","2018","Action,Comedy,Crime","2,10,1,10,6,2,5,7,4,9,10,1,5,6","MV5BMTg1MTY2MjYzNV5BMl5BanBnXkFtZTgwMTc4NTMwNDI@._V1_SY500_CR0,0,337,500_AL_.jpg","2018-02-28",0,"A group of friends who meet regularly for game nights find themselves trying to solve a murder mystery.","Rachel McAdams,Jesse Plemons,Json Bateman","https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into moviescoming values(2,"Area X","2018","Adventure,Drama,Fantasy","7,10,1,3,6,2,5,7,2,9,1,1,5,6","MV5BMTk2Mjc2NzYxNl5BMl5BanBnXkFtZTgwMTA2OTA1NDM@._V1_SY500_CR0,0,320,500_AL_.jpg","2018-02-23",0,"A biologist's husband disappears. She puts her name forward for an expedition into an environmental disaster zone, but does not find what she's expecting. ","Tessa Thompson,Jennifer json,Natalie Portman","https://images-na.ssl-images-amazon.com/images/M/MV5BMTk2Mjc2NzYxNl5BMl5BanBnXkFtZTgwMTA2OTA1NDM@._V1_SY500_CR0,0,320,500_AL_.jpg");

insert into moviescoming values(3,"Hannah","2017","Drama","6,7,1,3,6,2,5,7,2,9,1,1,5,6","MV5BNWJmMWIxMjQtZTk0Mi00YTE0LTkyNzAtYzQxYjcwYjE4ZDk2XkEyXkFqcGdeQXVyMTc4MzI2NQ@@._V1_SY500_SX350_AL_.jpg","2018-01-24",0,"Intimate portrait of a woman drifting between reality and denial when she is left alone to grapple with the consequences of her husband's imprisonment.","Charlotte Rampling","https://images-na.ssl-images-amazon.com/images/M/MV5BNWJmMWIxMjQtZTk0Mi00YTE0LTkyNMzI2NQ@@._V1_SY500_SX350_AL_.jpg");

insert into moviescoming values(4,"EndGame","2017","Action","10,7,1,3,6,2,5,7,9,1,1,5,6","MV5BNWJmMWIxMjQtZTk0Mi00YTE0LTkyNzAtYzQxYjcwYjE4ZDk2dfgfgfgqcGdeQXVyMTc4MzI2NQ@@._V1_SY500_SX350_AL_.jpg","2017-01-24",0,"Intimate portrait of a woman drifting bensequences of her husband's imprisonment.","Charlotte Rampling","https://images-na.ssl-images-amazon.com/images/M/MV5BNWJmMWIfgf00YTE0LTkyNMzI2NQ@@._V1_SY500_SX350_AL_.jpg");

//MoviesInTheaters

insert into moviesintheaters values(1,"Black Panther","2018","Action,Adventure,Sci-fy","9,10,1,9,6,1,5,7,4,9,10,1,5,10","MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg","2018-02-14",0,"After the events of Captain America: Civil War, King T'Challa returns home to the reclusive nation of Wakanda to serve as his country's new leader.","Chadwick Boseman,Michael B. Jordan,Lupita Nyong","https://images-na.ssl-images-amazon.com/images/M/MV5BMTg1MTY2MjYzNV5BMl5BanBnXkFtZTgwMTc4NTMwNDI@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into  moviesintheaters values(2,"Grottmannen Dug","2018","Animation,Fantasy","7,10,1,3,6,2,5,7,5,9,10,1,5,2","MV5BMTk2BanBnXkFtZTgwMTA2OTA1NDM@._V1_SY500_CR0,0,320,500_AL_.jpg","2018-03-23",0,"Set at the dawn of time, when prehistoric creatures and woolly mammoths roamed the earth, Early Man tells the story of Dug,","Tom Hiddlesto, json,Natalie Portman","https://images-na.ssl-images-amazon.com/images/M/MV5BYWMxYWVjNzAXkFqcGdeQXVyMjMxOTE0ODA@._V1_SY500_CR0,0,328,500_AL_.jpg");

insert into  moviesintheaters values(3,"Aiyaary","2018","Drama,Action","4,7,1,3,6,2,5,7,2,9,1,1,5,6","MV5BNWJmMWIxMjQtZTk0Mi00YTE0LTkyNzAtYzQxYjcwYjE4ZDk2XkEyXkFqcGdeQXVyMTc4MzI2NQ@@._V1_SY500_SX350_AL_.jpg","2018-02-16",0,"Intimate portrait of a woman drifting between reality and denial when she is left alone to grapple with the consequences of her husband's imprisonment.","Charlotte Rampling","Two officers with patriotic hearts suddenly have a fallout. The mentor, Colonel Abhay Singh has complete faith in the country's system");


// topratedindia 

insert into topratedindia values(1,"Anand","1971","Drama","9,10,1,9,6,1,5,7,4,9,10,1,5,10","MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg","1971-03-12",0,"A melodramatic tale of a man with a terminal disease. The story begins with Dr Bhaksar winning a literary prize for his book about a patient called Anand.","Rajesh Khanna","https://images-na.ssl-images-amazon.com/images/M/MV5BMTg1MTY2MjYzNV5BMl5BanBnXkFtZTgwMTc4NTMwNDI@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into  topratedindia values(2,"Dangal","2016","Biography,Drama","10,10,1,3,6,2,5,7,5,9,10,1,5,2","MV5BMTk2BanBnXkFtZTgwMTA2OTA1NDM@._V1_SY500_CR0,0,320,500_AL_.jpg","2016-03-23",0,"Biopic of Mahavir Singh Phogat, who taught wrestling to his daughters Babita Kumari and Geeta Phogat.","Aamir Khan,Fatima Sana Shaikh","https://images-na.ssl-images-amazon.com/images/M/MV5BYWMxYWVjNzAXkFqcGdeQXVyMjMxOTE0ODA@._V1_SY500_CR0,0,328,500_AL_.jpg");

insert into  topratedindia values(3,"Drishyam","2013","Drama","8,7,1,3,6,2,5,7,2,9,1,1,5,6,6,9","MV5BNWJmMWIxMjQtZTk0Mi00YTE0LTkyNzAtYzQxYjcwYjE4ZDk2XkEyXkFqcGdeQXVyMTc4MzI2NQ@@._V1_SY500_SX350_AL_.jpg","2013-02-1",0,"Georgekutty (Mohanlal) is a cable TV network owner in a remote and hilly village in Kerala. He lives a happy life with his wife and 2 girls.","Mohanlal","https://images-na.ssl-images-amazon.com/images/M/MV5BYmY3MzYwMGUtOWMxMTExNDQ2MTI@._V1_SX330_CR0,0,330,432_AL_.jpg");


//topratedmovies


insert into topratedmovies values(1,"Baazigar","1993","Crime,Drama","1,6,1,9,6,1,5,7,4,9,10,1,5,10","MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg","1997-02-15",0,"Widowed Madan Chopra lives a very wealthy lifestyle with two daughters, Seema and Priya.","Hrithik Roshan,Kajol","https://images-na.ssl-images-amazon.com/images/M/MV5BMTg1MTY2MjYzNV5BMlgff5BanBnXkFtZTgwMTc4NTMwNDI@._V1_SY500_CR0,0,337,500_AL_.jpg");

insert into  topratedmovies values(2,"Jodhaa Akbar","2008","Biography,Action","9,9,5,6,6,2,5,7,5,9,10,1,5,2","MV5BMTk2BanBnXkFtZTgwMTA2OTA1NDM@._V1_SY500_CR0,0,320,500_AL_.jpg","2008-02-15",0,"Jodhaa Akbar is a sixteenth century love story about a marriage of alliance that gave birth to true love between a great Mughal Emperor, Akbar and a Rajput princess, Jodhaa.","Hrithik Roshan,","https://images-na.ssl-images-amazon.com/images/M/MV5BYWMxYWVjNzAXkFqcGdeQXVyMjMxOTE0ODA@._V1_SY500_CR0,0,328,500_AL_.jpg");

insert into  topratedmovies values(3,"Wake Up Sid","2009","Drama","8,7,1,3,6,2,5,7,2,9,1,1,5,6,6,9","MV5BNWJmMWIxMjQtZTk0Mi00YTE0LTkyNzAtYzQxYjcwYjE4ZDk2XkEyXkFqcGdeQXVyMTc4MzI2NQ@@._V1_SY500_SX350_AL_.jpg","2009-06-5",0,""In Mumbai, Sid Mehra is, in the words of his father, an arrogant, spoiled brat. He lives with a doting mother","Ranbir Kapoor,Konkona Sen Sharma","https://images-na.ssl-images-amazon.com/images/M/MV5BYmY3MzYwMGUtOWMxMTExNDQ2MTI@._V1_SX330_CR0,0,330,432_AL_.jpg");
 
// favourite
insert into  favourite values(2,"Jodhaa Akbar","2008","Biography,Action","9,9,5,6,6,2,5,7,5,9,10,1,5,2","MV5BMTk2BanBnXkFtZTgwMTA2OTA1NDM@._V1_SY500_CR0,0,320,500_AL_.jpg","2008-02-15",0,"Jodhaa Akbar is a sixteenth century love story about a marriage of alliance that gave birth to true love between a great Mughal Emperor, Akbar and a Rajput princess, Jodhaa.","Hrithik Roshan,","https://images-na.ssl-images-amazon.com/images/M/MV5BYWMxYWVjNzAXkFqcGdeQXVyMjMxOTE0ODA@._V1_SY500_CR0,0,328,500_AL_.jpg");

insert into  favourite values(3,"Drishyam","2013","Drama","8,7,1,3,6,2,5,7,2,9,1,1,5,6,6,9","MV5BNWJmMWIxMjQtZTk0Mi00YTE0LTkyNzAtYzQxYjcwYjE4ZDk2XkEyXkFqcGdeQXVyMTc4MzI2NQ@@._V1_SY500_SX350_AL_.jpg","2013-02-1",0,"Georgekutty (Mohanlal) is a cable TV network owner in a remote and hilly village in Kerala. He lives a happy life with his wife and 2 girls.","Mohanlal","https://images-na.ssl-images-amazon.com/images/M/MV5BYmY3MzYwMGUtOWMxMTExNDQ2MTI@._V1_SX330_CR0,0,330,432_AL_.jpg");

