package com.greatelearning.factorydesignpattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import com.greatelearning.bean.MoviesComing;
import com.greatelearning.bean.TopRatedIndia;
import com.greatelearning.dao.MoviesTips;


class FactoryDesign {
	public static MoviesOnTipes getInstance(String type) {
		if(type.equalsIgnoreCase("moviescoming")) {
			return new MoviesComings();
		}else if(type.equalsIgnoreCase("moviesintheater")) {
			return new MoviesInTheater();
		}else if(type.equalsIgnoreCase("topratedmovies")) {
			return new TopRatedMovie();
		
		}else if(type.equalsIgnoreCase("favourites")) {
			return new Favourites();
		
		}else if(type.equalsIgnoreCase("topratedindia")) {
			return new TopRateIndia();
		}
			else {
			return null;
		}
	}
}

