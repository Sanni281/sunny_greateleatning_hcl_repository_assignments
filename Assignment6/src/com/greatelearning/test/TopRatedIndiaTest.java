package com.greatelearning.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatelearning.bean.TopRatedIndia;

public class TopRatedIndiaTest {

	@Test
	public void testSetId() {
		//fail("Not yet implemented");
		TopRatedIndia tri=new TopRatedIndia();
		tri.setId(4);
		assertTrue(tri.getId()==4);
	}

	@Test
	public void testSetTitle() {
		//fail("Not yet implemented");
		TopRatedIndia tri=new TopRatedIndia();
		tri.setTitle("hanah");
		assertTrue(tri.getTitle()=="hanah");
	}

	@Test
	public void testSetGenres() {
		//fail("Not yet implemented");
		TopRatedIndia tri=new TopRatedIndia();
		tri.setGenres("Action,Drama");
		assertTrue(tri.getGenres()=="Action,Drama");
	}

	@Test
	public void testSetPosterUrl() {
		//fail("Not yet implemented");
		TopRatedIndia tri=new TopRatedIndia();
		tri.setPosterUrl("https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");
		assertTrue(tri.getPosterUrl()=="https://images-na.ssl-images-amazon.com/images/M/MV5BMjQxMDE5NDg0NV5BMl5BanBnXkFtZTgwNTA5MDE2NDM@._V1_SY500_CR0,0,337,500_AL_.jpg");
	}

	@Test
	public void testGetAverageRating() {
		//fail("Not yet implemented");
		TopRatedIndia tri=new TopRatedIndia();
		tri.setAverageRating(0);
		assertTrue(tri.getAverageRating()==0);
	}

}
