package com.greatelearning.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatelearning.bean.MoviesComing;

public class MoviesComingTest {

	@Test
	public void testGetId() {
		//fail("Not yet implemented");
		MoviesComing mc=new MoviesComing();
		mc.setId(1);
		assertTrue(mc.getId()==1);
	}

	@Test
	public void testSetId() {
		//fail("Not yet implemented");
		MoviesComing mc=new MoviesComing();
		mc.setId(2);
		assertTrue(mc.getId()==2);
	
	}

	@Test
	public void testGetTitle() {
		//fail("Not yet implemented");
		MoviesComing mc=new MoviesComing();
		mc.setTitle("hanah");
		assertTrue(mc.getTitle()=="hanah");
	}

	@Test
	public void testSetTitle() {
		//fail("Not yet implemented");
		MoviesComing mc=new MoviesComing();
		mc.setTitle("hanah");
		assertTrue(mc.getTitle()=="hanah");
	}

	@Test
	public void testSetPoster() {
	//	fail("Not yet implemented");
		MoviesComing mc=new MoviesComing();
		mc.setPoster("MV5BNWJmMWIxMjQtZTk0Mi00YTE0LTkyNzAtYzQxYjcwYjE4ZDk2XkEyXkFqcGdeQXVyMTc4MzI2NQ@@._V1_SY500_SX350_AL_.jpg");
	assertTrue(mc.getPoster()=="MV5BNWJmMWIxMjQtZTk0Mi00YTE0LTkyNzAtYzQxYjcwYjE4ZDk2XkEyXkFqcGdeQXVyMTc4MzI2NQ@@._V1_SY500_SX350_AL_.jpg");
	
	}

	@Test
	public void testSetAverageRating() {
		//fail("Not yet implemented");
		MoviesComing mc=new MoviesComing();
		mc.setAverageRating(0);
		assertTrue(mc.getAverageRating()==0);
	}

}
