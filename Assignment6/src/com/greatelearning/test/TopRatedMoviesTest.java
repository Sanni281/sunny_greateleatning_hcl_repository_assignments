package com.greatelearning.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatelearning.bean.TopRatedMovies;

public class TopRatedMoviesTest {

	@Test
	public void testGetId() {
		//fail("Not yet implemented");
		TopRatedMovies trm=new TopRatedMovies();
		trm.setId(4);
		assertTrue(trm.getId()==4);
	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented");
		TopRatedMovies trm=new TopRatedMovies();
		trm.setYear("2018");
		assertTrue(trm.getYear()=="2018");
	}

	@Test
	public void testGetGenres() {
	//	fail("Not yet implemented");
		TopRatedMovies trm=new TopRatedMovies();
		trm.setGenres("Action,Drama,Adventure");
		assertTrue(trm.getGenres()=="Action,Drama,Adventure");
	}

	@Test
	public void testGetRating() {
		//fail("Not yet implemented");
		TopRatedMovies trm=new TopRatedMovies();
		trm.setRating("1,10,5,9,4,5,6,8,9");
		assertTrue(trm.getRating()=="1,10,5,9,4,5,6,8,9");
	}

	@Test
	public void testGetPoster() {
		//fail("Not yet implemented");
		TopRatedMovies trm=new TopRatedMovies();
		trm.setPoster("MV5BNWJmMWIxMjQtZTk0Mi00YTE0LTkyNzAtYzQxYjcwYjE4ZDk2XkEyXkFqcGdeQXVyMTc4MzI2NQ@@._V1_SY500_SX350_AL_.jpg");
		assertTrue(trm.getPoster()=="MV5BNWJmMWIxMjQtZTk0Mi00YTE0LTkyNzAtYzQxYjcwYjE4ZDk2XkEyXkFqcGdeQXVyMTc4MzI2NQ@@._V1_SY500_SX350_AL_.jpg");
		
	}

}
