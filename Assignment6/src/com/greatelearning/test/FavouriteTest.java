package com.greatelearning.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.greatelearning.bean.Favourite;
import com.greatelearning.bean.TopRatedMovies;

public class FavouriteTest {

	@Test
	public void testGetId() {
		//fail("Not yet implemented");
		Favourite ft=new Favourite();
		ft.setId(1);
		assertTrue(ft.getId()==1);
	}

	@Test
	public void testGetYear() {
		//fail("Not yet implemented");
		Favourite ft=new Favourite();
		ft.setYear("2018");
		assertTrue(ft.getYear()=="2018");
	}

	@Test
	public void testSetYear() {
		//fail("Not yet implemented");
		Favourite ft=new Favourite();
		ft.setYear("2019");
		assertTrue(ft.getYear()=="2019");
	}

	@Test
	public void testGetGenres() {
	//	fail("Not yet implemented");
		Favourite ft=new Favourite();
		ft.setGenres("Action,Drama");
		assertTrue(ft.getGenres()=="Action,Drama");
	}

	@Test
	public void testSetGenres() {
		//fail("Not yet implemented");
		Favourite ft=new Favourite();
		ft.setGenres("Action,Drama");
		assertTrue(ft.getGenres()=="Action,Drama");
	}

}
