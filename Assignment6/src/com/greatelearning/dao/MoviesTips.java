
package com.greatelearning.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.greatelearning.bean.Favourite;
import com.greatelearning.bean.MoviesComing;
import com.greatelearning.bean.MoviesInTheaters;
import com.greatelearning.bean.TopRatedIndia;
import com.greatelearning.bean.TopRatedMovies;
import com.greatelearning.resource.DbConnection;

public class MoviesTips {
	
	public List<MoviesComing> moviesComing() {
		List<MoviesComing> listOfmoviescoimg = new ArrayList<>();
		try {
		
			Connection con = DbConnection.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from moviescoming");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					MoviesComing m = new MoviesComing();
					m.setId(rs.getInt(1));
					m.setTitle(rs.getString(2));
					m.setYear(rs.getString(3));
					m.setGenres(rs.getString(4));
					m.setRating(rs.getString(5));
					m.setPoster(rs.getString(6));
					m.setReleaseDate(rs.getString(7));
					m.setAverageRating(rs.getInt(8));
					m.setStoryline(rs.getString(9));
					m.setActors(rs.getString(10));
					m.setPosterurl(rs.getString(11));
					listOfmoviescoimg.add(m);
			}
			} catch (Exception e) {
				System.out.println("Data not Display "+e);
			}
		return listOfmoviescoimg;
	}
	
	public List<MoviesInTheaters> moviesInTheater() {
		List<MoviesInTheaters> listOfMoviesInTheater = new ArrayList<>();
		try {
		
			Connection con = DbConnection.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from  moviesintheaters");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					MoviesInTheaters mt = new MoviesInTheaters();
					mt.setId(rs.getInt(1));
					mt.setTitle(rs.getString(2));
					mt.setYear(rs.getString(3));
					mt.setGenres(rs.getString(4));
					mt.setRating(rs.getString(5));
					mt.setPoster(rs.getString(6));
					mt.setReleaseDate(rs.getString(7));
					mt.setAverageRating(rs.getInt(8));
					mt.setStoryline(rs.getString(9));
					mt.setActors(rs.getString(10));
					mt.setPosterurl(rs.getString(11));
					listOfMoviesInTheater.add(mt);
			}
			} catch (Exception e) {
				System.out.println("Data not Display "+e);
			}
		return listOfMoviesInTheater;
	}
	public List<TopRatedMovies> topRatedMovies() {
		List<TopRatedMovies> listOfTopRatedMovies = new ArrayList<>();
		try {
		
			Connection con = DbConnection.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from topratedmovies");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					TopRatedMovies trm = new TopRatedMovies();
					trm.setId(rs.getInt(1));
					trm.setTitle(rs.getString(2));
					trm.setYear(rs.getString(3));
					trm.setGenres(rs.getString(4));
					trm.setRating(rs.getString(5));
					trm.setPoster(rs.getString(6));
					trm.setReleaseDate(rs.getString(7));
					trm.setAverageRating(rs.getInt(8));
				trm.setStoryLine(rs.getString(9));
					trm.setActors(rs.getString(10));
				trm.setPosterUrl(rs.getString(11));
				listOfTopRatedMovies.add(trm);
			}
			} catch (Exception e) {
				System.out.println("Data not Display "+e);
			}
		return listOfTopRatedMovies;
	}
	public List<Favourite> favourite() {
		List<Favourite> listOfFavouriteMovies = new ArrayList<>();
		try {
		
			Connection con = DbConnection.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from  favourite");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					Favourite mt = new Favourite();
					mt.setId(rs.getInt(1));
					mt.setTitle(rs.getString(2));
					mt.setYear(rs.getString(3));
					mt.setGenres(rs.getString(4));
					mt.setRating(rs.getString(5));
					mt.setPoster(rs.getString(6));
					mt.setReleaseDate(rs.getString(7));
					mt.setAverageRating(rs.getInt(8));
					mt.setStoryLine(rs.getString(9));
					mt.setActors(rs.getString(10));
					mt.setPosterUrl(rs.getString(11));
					listOfFavouriteMovies.add(mt);
			}
			} catch (Exception e) {
				System.out.println("Data not Display "+e);
			}
		return listOfFavouriteMovies;
	}
	public List<TopRatedIndia> topRatedIndia() {
		List<TopRatedIndia> listOfTopRatedIndia = new ArrayList<>();
		try {
		
			Connection con = DbConnection.getDbConnection();
			PreparedStatement pstmt = con.prepareStatement("select * from topratedindia");
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
					TopRatedIndia trm = new TopRatedIndia();
					trm.setId(rs.getInt(1));
					trm.setTitle(rs.getString(2));
					trm.setYear(rs.getString(3));
					trm.setGenres(rs.getString(4));
					trm.setRating(rs.getString(5));
					trm.setPoster(rs.getString(6));
					trm.setReleaseDate(rs.getString(7));
					trm.setAverageRating(rs.getInt(8));
				trm.setStoryLine(rs.getString(9));
					trm.setActors(rs.getString(10));
				trm.setPosterUrl(rs.getString(11));
				listOfTopRatedIndia.add(trm);
			}
			} catch (Exception e) {
				System.out.println("Data not Display "+e);
			}
		return listOfTopRatedIndia;
	}
	

}
