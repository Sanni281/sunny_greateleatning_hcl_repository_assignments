package com.greatelearning.bean;

public class MoviesInTheaters {
	private int id;
	private String title;
	private String year;
	private String genres;
	private String rating;
	private String poster;
	private String storyline;
	private String posterurl;
	private int averageRating;
private String actors;
	private String releaseDate;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getGenres() {
		return genres;
	}
	public void setGenres(String genres) {
		this.genres = genres;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getPoster() {
		return poster;
	}
	public void setPoster(String poster) {
		this.poster = poster;
	}
	public String getStoryline() {
		return storyline;
	}
	public void setStoryline(String storyline) {
		this.storyline = storyline;
	}
	public String getPosterurl() {
		return posterurl;
	}
	public void setPosterurl(String posterurl) {
		this.posterurl = posterurl;
	}
	public int getAverageRating() {
		return averageRating;
	}
	public void setAverageRating(int averageRating) {
		this.averageRating = averageRating;
	}
	public String getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
	public String getActors() {
		return actors;
	}
	public void setActors(String actors) {
		this.actors = actors;
	}
	@Override
	public String toString() {
		return "MoviesInTheaters [id=" + id + ", title=" + title + ", year=" + year + ", genres=" + genres + ", rating="
				+ rating + ", poster=" + poster + ", storyline=" + storyline + ", posterurl=" + posterurl
				+ ", averageRating=" + averageRating + ", actors=" + actors + ", releaseDate=" + releaseDate + "]";
	}

}
