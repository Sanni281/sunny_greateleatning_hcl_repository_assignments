package com.greatelearning.bean;

public class Favourite {
	private int id;
	private String title;
	private String year;
      private String genres;
	private String rating;
	private String poster;
	private String storyLine;
	private String posterUrl;
	private String actors;
	private int averageRating;
	private String releaseDate;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getGenres() {
		return genres;
	}
	public void setGenres(String genres) {
		this.genres = genres;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}
	public String getPoster() {
		return poster;
	}
	public void setPoster(String poster) {
		this.poster = poster;
	}
	public String getStoryLine() {
		return storyLine;
	}
	public void setStoryLine(String storyLine) {
		this.storyLine = storyLine;
	}
	public String getPosterUrl() {
		return posterUrl;
	}
	public void setPosterUrl(String posterUrl) {
		this.posterUrl = posterUrl;
	}
	public int getAverageRating() {
		return averageRating;
	}
	public void setAverageRating(int averageRating) {
		this.averageRating = averageRating;
	}
	public String getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(String releaseDate) {
		this.releaseDate = releaseDate;
	}
	public String getActors() {
		return actors;
	}
	public void setActors(String actors) {
		this.actors = actors;
	}
	@Override
	public String toString() {
		return "Favourite [id=" + id + ", title=" + title + ", year=" + year + ", genres=" + genres + ", rating="
				+ rating + ", poster=" + poster + ", storyLine=" + storyLine + ", posterUrl=" + posterUrl + ", actors="
				+ actors + ", averageRating=" + averageRating + ", releaseDate=" + releaseDate + "]";
	}
	
}
